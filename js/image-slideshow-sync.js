/**
 * @file
 * Js File.
 */

(function ($) {
  $(document).ready(function () {
    // Gallery Slider Starts

    $('.image_slideshow_sync-slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      fade: false,
      adaptiveHeight: true,
      infinite: true,
      useTransform: true,
      speed: 400,
      autoplay: true,
      cssEase: 'cubic-bezier(0.77, 0, 0.18, 1)',
      responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: true,
          mobileFirst: true,
        }
      },
        {
          breakpoint: 680,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            mobileFirst: true,
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            mobileFirst: true,
          }
        }
      ]
    });

    $('.image_slideshow_sync-nav')
      .on('init', function (event, slick) {
        $('.image_slideshow_sync-nav .slick-slide.slick-current').addClass('is-active');
      })
      .slick({
        slidesToShow: 9,
        slidesToScroll: 9,
        dots: false,
        focusOnSelect: false,
        infinite: true,
        arrows: false,
        responsive: [{
          breakpoint: 1024,
          settings: {
            slidesToShow: 5,
            slidesToScroll: 5,
          }
        }, {
          breakpoint: 640,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 4,
          }
        }, {
          breakpoint: 420,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
          }
        }]
      });

    $('.image_slideshow_sync-slider').on('afterChange', function (event, slick, currentSlide) {
      $('.image_slideshow_sync-nav').slick('slickGoTo', currentSlide);
      var currrentNavSlideElem = '.image_slideshow_sync-nav .slick-slide[data-slick-index="' + currentSlide + '"]';
      $('.image_slideshow_sync-nav .slick-slide.is-active').removeClass('is-active');
      $(currrentNavSlideElem).addClass('is-active');
    });

    $('.image_slideshow_sync-nav').on('click', '.slick-slide', function (event) {
      event.preventDefault();
      var goToSingleSlide = $(this).data('slick-index');

      $('.image_slideshow_sync-slider').slick('slickGoTo', goToSingleSlide);
    });

  });
}(jQuery));
